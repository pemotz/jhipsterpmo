export interface IStudent {
    id?: number;
    martrikelnummer?: number;
    vorname?: string;
    nachname?: string;
    adresse?: string;
    postleitzahl?: string;
    ort?: string;
}

export class Student implements IStudent {
    constructor(
        public id?: number,
        public martrikelnummer?: number,
        public vorname?: string,
        public nachname?: string,
        public adresse?: string,
        public postleitzahl?: string,
        public ort?: string
    ) {}
}
